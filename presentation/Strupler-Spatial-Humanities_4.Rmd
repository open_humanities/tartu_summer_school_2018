---
title: "</br> Spatial Humanities <br>"
subtitle: "</br> A practical introduction with R"
author: "Néhémie Strupler"
date: "2018"
output:
  xaringan::moon_reader:
    seal: true
    self_contained: true
    css: ["./css/default.css", "./css/my-themes.css"]
    nature:
      highlightStyle: dracula
      highlightLines: true
      ratio: '4:3'
      countIncrementalSlides: true
---
layout: true

```{r r-setup, include=FALSE, echo = FALSE}
  options(htmltools.dir.version = FALSE)
  knitr::opts_chunk$set(cache = TRUE, fig.width = 12, fig.align = "center")
  knitr::opts_knit$set(global.par = TRUE)
  knitr::opts_chunk$set(message = FALSE)
  opar <- par(no.readonly = TRUE)
  par(mar = rep(0, 4))
```

```{r load_refs, echo=FALSE, cache=FALSE}
  library(RefManageR)
  BibOptions(check.entries = FALSE,
            bib.style = "authoryear",
            cite.style = 'Chicago',
            style = "markdown",
            hyperlink = FALSE,
            dashed = FALSE)
  bib <- ReadBib("./biblio.bib", check = FALSE)
```

---
class:inverse, middle, center


# Satellite imagery in R



---

## Prerequisites

--

 - ESA Copernicus hub [https://scihub.copernicus.eu/](https://scihub.copernicus.eu/)
 
--

## Problem

 - Very big...

--


```{r}
library(rgdal)
library(raster)
```

```{r}
coordinate_tartu <- c(483771, 6471051)

extent <- extent( coordinate_tartu[c(1,1,2,2)] + c(-5000, 5000))
```


```{r}
img_path <-
  paste0("./data/",
         "S2B_MSIL1C_20180823T094029_N0206_R036_T35VME_20180823T114359.SAFE/",
         "GRANULE/",
         "L1C_T35VME_A007641_20180823T094320/",
         "IMG_DATA/")
```

--


---


```{r}
list.files(img_path)
```

--

```{r}
bandn <- c(2:4)
bands <- paste0("band", bandn)
pathbands <- paste0(img_path,
                    "T35VME_20180823T094029_B0",
                    bandn, ".jp2")
```

See https://earth.esa.int/web/sentinel/user-guides/sentinel-2-msi/resolutions/spatial
---

```{r}
raster_tartu <- stack(pathbands)
names(raster_tartu) <- bands
raster_tartu <- crop(raster_tartu, extent)
plotRGB(raster_tartu, 3, 2, 1, stretch = "lin")
```

---

```{r, eval = FALSE}
writeRaster(raster_tartu,
            filename = "raster_tartu.tif",
            options = "INTERLEAVE=BAND",
            overwrite = TRUE)

tiff("raster_tartu.tiff", width = 980, height = 722)
plotRGB(raster_tartu, 3, 2, 1, stretch = "lin", interpolate = TRUE)
dev.off()
```

---

class:inverse, middle


# Final work

 - Make a mapview plot with 4 synchronised panels about Tartu
   + counties
   + municipalities
   + settlements as points
   + raster
