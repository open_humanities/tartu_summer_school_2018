---
title: "</br> Spatial Humanities <br>"
subtitle: "</br> A practical introduction with R - First steps"
author: "Néhémie Strupler"
date: "2018"
output:
  xaringan::moon_reader:
    seal: true
    self_contained: true
    css: ["./css/default.css", "./css/my-themes.css"]
    nature:
      highlightStyle: dracula
      highlightLines: true
      ratio: '4:3'
      countIncrementalSlides: true
---
layout: true

```{r r-setup, include=FALSE, echo = FALSE}
  options(htmltools.dir.version = FALSE)
  knitr::opts_chunk$set(cache = TRUE, fig.width = 12, fig.align = "center")
  knitr::opts_knit$set(global.par = TRUE)
  opar <- par(no.readonly = TRUE)
  par(mar = rep(0, 4))
```

```{r load_refs, echo=FALSE, cache=FALSE}
  library(RefManageR)
  BibOptions(check.entries = FALSE,
            bib.style = "authoryear",
            cite.style = 'Chicago',
            style = "markdown",
            hyperlink = FALSE,
            dashed = FALSE)
  bib <- ReadBib("./biblio.bib", check = FALSE)
```

---
class:inverse, middle, center

# Practice in R


???
---

## Prerequisites

--

 - [Up-to-date version of R](https://cran.r-project.org/)
 - IDE [(Rstudio)](https://www.rstudio.com/products/rstudio/download/#download)

--

### Package requirements (in R)

 - install the packages we need

--

```{r, eval=FALSE}
install.packages("sf")
```

--

> On Mac and Linux a few dependencies must be installed for **sf**.
> See the package's README at
> [github.com/r-spatial/sf](https://github.com/r-spatial/sf).
> Main dependencies are:
>
> - GDAL
> - GEOS
> - Proj
>

.caption[On these: `r Citet(bib, "McInerney2015opensource", .opts = list(hyperlink = "to.doc"))`]






---

```{r, eval=FALSE}
install.packages("raster")
install.packages("spData")
install.packages("cartography")
install.packages("mapview")
install.packages("ggplot2")
```

--

Packages are attached with the `library()` function (if installed) as
follows:

```{r, message=FALSE}
library(sf)
library(raster)
library(spData)

library(magrittr)    # %>%
```

--

Most of the packages have a "vignette" to introduce main fuctions,
such as:


```{r, eval=FALSE}
vignette(package = "sf", "sf1")
vignette(package = "raster", "Raster")
```

<iframe
src="https://cran.r-project.org/web/packages/sf/vignettes/sf1.html"
width="100%" height="200" border="0" ></iframe>


---
**First plot**

--

```{r first plot, warning=FALSE}
world %>% plot # plot(world)
```





---

.code60[
```{r first_print}
world[1:3, ]
```
]
--

.code60[
```{r first_str}
world %>% str
```
]



---

Use the help to learn about the different arguments

```{r, eval=FALSE}
?plot
```
--

such as `max.plot`

--

```{r only_one_plot}
world %>% plot(max.plot = 1)
```




---

Subset the data…

--

```{r four_plots}
world[7:10] %>% plot
```




---

… like a data.frame

--

```{r wgs84}
world[world$continent == "Europe", "gdpPercap"] %>% plot
```





---
The sf class simplify the data if you subset with the operator `$`

--

```{r dollar_subset}
world$gdpPercap %>% head
```

--

```{r dollar_subset_fig, fig.height=4}
world$gdpPercap %>% plot
```

--

```{r dollar_subset_class}
world["gdpPercap"] %>% class
```



---
Change coordinate system

--
```{r estonian crs}
world %<>% st_transform(crs = 3300)
world[world$continent == "Europe", "gdpPercap"] %>% plot

```




---
class: inverse, center middle

# Under the hood

## The vector world





---
```{r lib_sf, eval=FALSE}
library(sf)          # classes and functions for vector data
```

--

**S**imple **f**eature is a standard (ISO 19125-1:2004).

A feature

--
 - is a thing (a tree, a building, a city)
--

 - may be part of a collection (forest = trees)
--

 - has a *geometry* to indicate its location and form
--

 - all geometries are composed of points
--

 - has attributes to describe its proprieties (color, name, ...)





---

![](img/2-r/sf-sf1-geometries.png)

.my-caption[ [`sf` - vignette 1 | GPL-2 ](https://cran.r-project.org/web/packages/sf/vignettes/sf1.html)]


???
---

### In practice

- Functions and methods in sf are prefixed by st_ (=*spatial and temporal*)
```{r st_, eval=FALSE}
st_*fun*
```

--

- Attributes are stored in data.frame objects

--

- geometries are put in a list-column

--
>The three classes used to represent simple features are:
>
> - **sf**, the table (data.frame) with feature attributes and feature
> geometries, which contains
> - **sfc**, the list-column with the geometries for each feature (record), which is composed of
> - **sfg**, the feature geometry of an individual simple feature.
>
> (`sf`, vignette "Simple Features for R")





---

**Bottom up: Point**

--

```{r bottom_up_sf_pt}
p_1 <- st_point( c(1, 2))
class(p_1)
```
--

```{r bottom_up_sf_pt_2}
p_2 <- st_point(c(2, 1))
pts <- list(p_1, p_2)
pts <- st_sfc(pts, crs = 4326 )
class(pts)
```
--

```{r bottom_up_sf_pt_3}
pts <- st_sf(data.frame("col" = c("black", "red"),
                        "type" = c("big", "small"),
                        "geometry" = pts))
class(pts)
st_bbox(pts)
```

---

**Line**

--

```{r bottom_up_sf_ln}
pts <-  matrix(c(1, 10, 5, 25, 10, 10),
               ncol = 2, byrow = TRUE)
pts
```

--

```{r bottom_up_sf_ln_2, fig.height=4}
ln <- st_linestring(pts)
plot(ln)
```

---

**Polygon**

--

```{r bottom_up_sf_pl}
outer <- matrix(c(0, 0, 10, 0, 10, 10, 0, 10, 0, 0),
                ncol = 2,
                byrow = TRUE)

pl1 = st_polygon(list(outer))
```

--

```{r bottom_up_sf_pl_2, fig.height=4}

plot(pl1, lwd = 4)
```

---
class: inverse, center, middle

# Your turn



---

1. What does `st_geometry(world)` return?
--

2. What does `st_geometry(world) <- NULL` do? # Reset with `data(world)`
--

3. Why some say that "[The geometry column is
   sticky](https://github.com/jannes-m/erum18_geocompr/blob/master/pres/pdf/02_vector.pdf)"
--

4. Find another possibility to achieve the same as 2.
--

5. Reproduce this
--


```{r exexrcie_map, echo=FALSE, fig.height=4, fig.width=12}
data(world)
plot(st_geometry(world), reset=FALSE)
plot(world[world$name_long == "China", 1], add = TRUE,
     col = "red")
```
Solution:
--

```{r world_as_df, eval = FALSE}
as.data.frame(world)[1, 1:10]
world$geom <- NULL
data(world)

plot(st_geometry(world), reset=FALSE)
plot(world[world$name_long == "China", 1], add = TRUE, col = "red")
```





---
class: inverse, center middle

# Under the hood

## The raster world




---

```{r raster_logo}
library(raster) #  RasterLayer, RasterBrick, and RasterStack Classes

logo <- raster(system.file("external/rlogo.grd", package="raster")) 
logo
```

???
---

```{r raster_logo_plot}
color_ramp <- colorRampPalette(c("black", "white"))(255)
plot(logo, col = color_ramp)
```




---
```{r raster_hist}
hist(logo)
```





---


```{r raster_logo_fun}
crazy_logo <- logo
values(crazy_logo) <- sin(values(crazy_logo))
plot(crazy_logo, col = color_ramp)
```

---


```{r raster_logo_fun_2}
focal_logo = focal(logo, w = matrix(1, nrow = 5, ncol = 5), fun = mean)
plot(focal_logo, col = color_ramp)
```



---

```{r raster_logo_fun_3}
apollo <- raster("img/0-intro/Aldrin_Apollo_11_wikipedia.jpg") 
apollo_logo <- focal(apollo, w = matrix(1, nrow = 25, ncol = 1),
                     fun = min)
par(mfrow = c(1,2))
plot(apollo, col = color_ramp)
plot(apollo_logo, col = color_ramp)
```



---
class: inverse, center, middle

# Practice!





---

Estonian Data: https://geoportaal.maaamet.ee/eng/Maps-and-Data/Administrative-and-Settlement-Division-p312.html

--

 - counties.shp
 - Settlements.shp


<iframe
src="https://geoportaal.maaamet.ee/eng/Maps-and-Data/Administrative-and-Settlement-Division-p312.html"
width="100%" height="400" border="0" ></iframe>


---
Read the data

```{r read estonian data}
counties <- st_read(dsn = "data/estonia/maakond_20180701.shp")
counties <- counties[order(counties$MNIMI), ]
```

---

```{r plot estonian data}
plot(counties[1],
     key.pos=2,
     key.length = 0.618,
     key.width = lcm(5.8),
     main = "Counties")
```




---

```{r read estonian data_2}
settlements <- st_read(dsn = "data/estonia/asustusyksus_20180701.shp")
```

---

```{r plot estonian data_2}
plot(settlements["TYYP"])
```





---


```{r estonia_relevel_tyyp}
levels(settlements$TYYP) <- c("village", "town",
                              "town w/o municipal status", "urban regions",
                              "township", "hamlet")
```
--

```{r estonia_releveled, fig.width=10}
plot(settlements["TYYP"],
     key.pos=2, key.length = 0.418, key.width = lcm(6.5),
     main = "Settlements")
```


---
class:inverse, middle

## Your turn!

 1. How many settlements by counties?


---

```{r estonian_centroid}
settlements_center <- st_centroid(settlements)
```
--

```{r estonian_intersect}
countinter  <- st_contains(counties, settlements_center)
settlements_nb <- sapply(countinter, length)
counties$settlements_nb <- settlements_nb
```

---

```{r estonian_intersect_plot, fig.height=5}
plot(counties["settlements_nb"],
     main = "settlements by counties")
```

---

```{r estonian_intersect_control, fig.width=12}
plot(settlements["MNIMI"],
     key.pos=2,
     key.length = 0.418,
     key.width = lcm(6.5),
     main = "Settlements")
```




---

.code70[

```{r estonian_intersect_diff}
table(settlements$MNIMI)

as.data.frame(counties[c("MNIMI", "settlements_nb")])[1:2]
```

]





---

```{r estonian_intersect_diff_plot}
# Find the difference
lost_settlements <- setdiff(rownames(settlements), unlist(countinter))
settlements[lost_settlements, 1] %>% plot
```




---

```{r estonian_intersect_show_diff}
settlements[lost_settlements[3], 1] %>% plot(reset=FALSE )
plot(settlements[700:1020,1], add = TRUE)
plot(settlements_center[1], add = TRUE, pch=19, col = "red")

```

---

```{r benchmark with centroid, warning=FALSE}
system.time({ st_contains(counties, st_centroid(settlements)) })
```

--

```{r benchmark without centroid, warning=FALSE}
system.time({ st_contains(counties, settlements) })
```

---
class:inverse

Your turn!

 1. How many settlements (ANIMI) by municipalities (ONIMI)?

--

 2. Create a new sf object with municipalities


---


```{r municipalities}
omini <- aggregate(ANIMI ~ ONIMI, data=settlements, FUN = length)
knitr::kable(omini, format='html')
```

---


```{r municipalities_2}
omini_sf <- aggregate(settlements["ANIMI"],
                      by = list(settlements$ONIMI), FUN = length)
names(omini_sf)[1:2] <- c("ONIMI", "settlements_nb")
plot(omini_sf["settlements_nb"])
```




---

```{r municipalities_3, fig.width=12, dpi=300, echo=FALSE}
library(classInt)
omini <- omini[order(omini$ANIMI), ]
par(mar = c(0, 2, 4, 0))
bp_col <- findColours(clI = classIntervals(omini$ANIMI, n = 9, style = "pretty"),
                      pal = sf.colors(9))
bp <- omini$ANIMI %>% barplot(col = bp_col,
                              main = "Number of settlements by municipalities")
text(x = bp, y = omini$ANIMI, labels = omini$ONIMI,
     cex = 0.6, srt=-45, adj = 1.2, xpd = TRUE)
```




---
class: inverse

### A package to make nice plot

--

```{r carto_load}
library(cartography)
```

--

N.B. What we draw are choropleth map

 - Depict the distribution of data classes in well
   defined areas (polygons)
 - gives a sense of patterning and good basis for comparaison

--

Cartography makes it easier to produce publication ready plots


???
---


```{r carto_intro, eval=FALSE}
choroLayer(x = omini_sf,
           var = "settlements_nb",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "wine.pal", n1 = 8),
           legend.title.txt = "",
           legend.pos = 'topleft')
# add a title and layout
layoutLayer(title = "Number of settlements by municipalities since the reform of october 2017",
            theme = "wine.pal", frame =F, north = TRUE,
            sources = "data: https://geoportaal.maaamet.ee",
            author = "author: Néhémie (with R, sf & cartography)",
            scale = 50)
```

---

```{r carto_intro_2, echo=FALSE}
choroLayer(x = omini_sf,
           var = "settlements_nb",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "wine.pal", n1 = 8),
           legend.title.txt = "",
           legend.pos = 'topleft')
# add a title and layout
layoutLayer(title = "Number of settlements by municipalities since the reform of october 2017",
            theme = "wine.pal", frame =F, north = TRUE,
            sources = "data: https://geoportaal.maaamet.ee",
            author = "author: Néhémie (with R, sf & cartography)",
            scale = 50)
```




---
## Export maps

Functions to export a map and save it

```{r export, eval = FALSE}
bmp(filename = "Rplot.bmp", width = 480, height = 480)

jpeg(filename = "Rplot.jpeg", width = 480, height = 480)

png(filename = "Rplot.png", width = 480, height = 480)

tiff(filename = "Rplot.tiff", width = 480, height = 480)

svg(filename = "Rplot.svg", width = 7, height = 7)

pdf(filename = "Rplot.svg", width = 7, height = 7)
```

--
You need to end plotting with dev.off

```{r export-dev-off, eval = FALSE}
dev.off()
```




---

```{r export-example, eval = FALSE}
pdf("my_woderful_plot.pdf")

choroLayer(x = omini_sf,
           var = "settlements_nb",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "wine.pal", n1 = 8),
           legend.title.txt = "",
           legend.pos = 'topleft')
# add a title and layout
layoutLayer(title = "Number of settlements by municipalities since the reform of october 2017",
            theme = "wine.pal", frame =F, north = TRUE,
            sources = "data: https://geoportaal.maaamet.ee",
            author = "author: Néhémie (with R, sf & cartography)",
            scale = 50)

dev.off()
```





---

### A word on classification:

--

 - with `sf` argument *breaks*
 - with `cartography` argument  *method*

--

```{r classification, fig.height=7, fig.width=12, echo = FALSE}
par(mfcol = c(3, 2), mar = c(0, 0, 1, 0))
choroLayer(omini_sf, var = "settlements_nb", method = "equal",
           legend.pos = 'topleft')
  title(main = "classifcation 'equal'")
choroLayer(omini_sf, var = "settlements_nb", method = "sd",
           legend.pos = 'topleft')
  title(main = "classifcation 'sd'")
choroLayer(omini_sf, var = "settlements_nb", method = "msd",
           legend.pos = 'topleft')
  title(main = "'msd'")
choroLayer(omini_sf, var = "settlements_nb", method = "geom",
           legend.pos = 'topleft')
  title(main = "'geom'")
choroLayer(omini_sf, var = "settlements_nb", method = "arith",
           legend.pos = 'topleft')
  title(main = "'arith'")
choroLayer(omini_sf, var = "settlements_nb", method = "quantile",
           legend.pos = 'topleft')
  title(main = "'quantile'")
```

???
---
class: inverse, middle

# Merging data

- Add the population by county


---
Getting Demographic data: http://pub.stat.ee

--

RV022: RAHVASTIK SOO, VANUSERÜHMA JA MAAKONNA JÄRGI, 1. JAANUAR

<iframe
src="http://pub.stat.ee/px-web.2001/Dialog/varval.asp?ma=RV022&ti=RAHVASTIK+SOO%2C+VANUSER%DCHMA+JA+MAAKONNA+J%C4RGI%2C+1%2E+JAANUAR&path=../Database/Rahvastik/01Rahvastikunaitajad_ja_koosseis/04Rahvaarv_ja_rahvastiku_koosseis/&lang=2"
width="90%" height="70%" border="0" ></iframe>

> Tabeldieraldusega pealkirjata tekst (.csv) 

???
---

```{r demos_wrangling}
demos <- read.delim("data/estonia/RV022m.csv", header=FALSE)
demos$MNIMI <- counties$MNIMI
demos$pop <- demos$V4
demos <- demos[c("MNIMI", "pop")]
sum(demos$pop)
knitr::kable(demos, format = 'html')
```






---

.code80[
```{r demos_merge}
counties <- merge(counties, demos)
counties[1:3, ]
```
]



---

.code80[
```{r demos_merge_area}
counties$area_km2 <- st_area(counties)/1000000
counties[1:3, ]
```
]




---
class: inverse

### Your turn!

 - Make a plot of the density of population by counties

---

.code80[
```{r demos_solution, eval=FALSE}
counties$density <- counties$pop / counties$area_km2
#With cartography
choroLayer(x = counties,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "taupe.pal", n1 = 8),
           legend.title.txt = "in/km^2",
           legend.pos = 'topleft')
# add a title and layout
layoutLayer(title = "Density of population by counties in 2017",
            theme = "pastel.pal", frame =F, north = TRUE,
            sources = "data: https://geoportaal.maaamet.ee & http://pub.stat.ee/",
            author = "author: Néhémie (with R, sf & cartography)",
            scale = 50)
```
]

---

```{r demos_solution_2, echo=FALSE}
counties$density <- counties$pop / counties$area_km2
#With cartography
choroLayer(x = counties,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "taupe.pal", n1 = 8),
           legend.title.txt = "in/km^2",
           legend.pos = 'topleft')
# add a title and layout
layoutLayer(title = "Density of population by counties in 2017",
            theme = "pastel.pal", frame =F, north = TRUE,
            sources = "data: https://geoportaal.maaamet.ee & http://pub.stat.ee/",
            author = "author: Néhémie (with R, sf & cartography)",
            scale = 50)
```


---

## Adding labels to your plots with cartography


```{r demos_with_layout, eval = FALSE}
# Background
choroLayer(x = counties,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "turquoise.pal", n1 = 8),
           legend.title.txt = "inhabitants per sqm",
           legend.pos = 'topleft')
layoutLayer(title = "Most Populated Counties of Estonia",
            author = "Apollo",
            sources = "data: https://geoportaal.maaamet.ee & https://pub.stat.ee/",
            col = NA,
            coltitle = "black",
            )
# Label plot
labelLayer(spdf = counties,
           txt = "MNIMI", # label field in df
           col = "darkred", # color of the labels
           cex = 0.7,
           halo = TRUE)
```

---
```{r demos_with_layout_2, echo = FALSE}
# Background
choroLayer(x = counties,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "turquoise.pal", n1 = 8),
           legend.title.txt = "inhabitants per sqm",
           legend.pos = 'topleft')
layoutLayer(title = "Most Populated Counties of Estonia",
            author = "Apollo",
            sources = "data: https://geoportaal.maaamet.ee & https://pub.stat.ee/",
            col = NA,
            coltitle = "black",
            )
# Label plot
labelLayer(spdf = counties,
           txt = "MNIMI", # label field in df
           col = "darkred", # color of the labels
           cex = 0.7,
           halo = TRUE)
```


---
## Get tiles 


```{r demos_with_layout_3, eval = FALSE, fig.width=5}
# Background
EUosm <- getTiles(counties, type = "cartodark")
# Layout plot
tilesLayer(EUosm)
choroLayer(x = counties,
           add = TRUE,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "turquoise.pal", n1 = 8),
           legend.title.txt = "inhabitants per sqkm",
           legend.pos = 'bottomleft')
```


---


```{r demos_with_layout_4, echo = FALSE}
# Background
EUosm <- getTiles(counties, type = "cartodark")
# Layout plot
tilesLayer(EUosm)
choroLayer(x = counties,
           add = TRUE,
           var = "density",
           method = "fisher-jenks",
           nclass = 8,
           col = carto.pal(pal1 = "turquoise.pal", n1 = 8),
           legend.title.txt = "inhabitants per sqkm",
           legend.pos = 'bottomleft')
```

---
class: inverse

## Practice

--

- Compute the median of settlement size by counties

--

.code80[
```{r settlement_surface, eval = FALSE}
settlements$area_km2 <- st_area(settlements)/1000000
par(mar = c(2, 10, 2, 2))
set_median <-  aggregate(settlements$area_km2,
                         list(settlements$MNIMI), FUN = median)
names(set_median)[1:2] <- c("MNIMI", "settlements_median_surface")
counties <- merge(counties, set_median)

# Or look at the boxplot
boxplot(as.vector(settlements$area_km2) ~ settlements$MNIMI, las = 1,
        horizontal = TRUE)

```
]


---


```{r settlement_surface_2, echo = FALSE}
settlements$area_km2 <- st_area(settlements)/1000000
par(mar = c(2, 10, 2, 2))
set_median <-  aggregate(settlements$area_km2,
                         list(settlements$MNIMI), FUN = median)
names(set_median)[1:2] <- c("MNIMI", "settlements_median_surface")
counties <- merge(counties, set_median)

# Or look at the boxplot
boxplot(as.vector(settlements$area_km2) ~ settlements$MNIMI, las = 1,
        horizontal = TRUE)

```
