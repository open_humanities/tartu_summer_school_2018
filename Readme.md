This repository provides material as well as supplementary data
the workshop **Practical Introduction to Spatial Humanities with R**
that was held during the Summer School [Digital Methods in Humanities and Social
Sciences](https://digitalmethods.ut.ee/), August 21-25, 2018,
University of Tartu, Estonia
